﻿/*

В строке записаны слова, разделённые пробелами в произвольном количестве.
Сжатие текста состоит в том, что между словами оставляется по одному пробелу, а после последнего слова пробелы удаляются
(пробелы перед первым словом сохраняются).
Если строка содержит только пробелы, то все сохраняются.

Написать функцию сжимающую текст.


*/

#include <iostream>
#include <string>
#include <sstream>

std::string compressText(const std::string& text) {
    std::istringstream iss(text);
    std::string word;
    std::string result;

    while (iss >> word)
    {
        result += word + " ";
    }

    if (!result.empty())
    {
        result.pop_back();
    }
    return result;
}

int main()
{
    std::string text = "  Hello  world!   ";

    std::string compressedText = compressText(text);

    std::cout << "Original text: \"" << text << "\"\n";
    std::cout << "Compressed text: \"" << compressedText << "\"\n";

    return 0;
}
